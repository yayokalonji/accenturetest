package com.test.accenturetest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.accenturetest.model.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/getAll")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getAllPersonsAPI() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/getAll")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getUserByFirstName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/get?firstName={firstName}", "Lisa")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createPersonAPI() throws Exception {
        Person person1 = new Person("Lisa", "Simpson", 11);
        person1.setId("2463429hfjkfh");
        mockMvc.perform(MockMvcRequestBuilders
                .post("/create?firstName=={firstName}&lastName={lastName}&age={age}", "Lisa", "Simpson", 11)
                .content(asJsonString(person1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }

    @Test
    public void deletePersonAPI() throws Exception {
       /* mockMvc.perform(MockMvcRequestBuilders.delete("/delete?firstName={firstName}", "Lisa"))
                .andExpect(status().is(200));*/
    }


    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
