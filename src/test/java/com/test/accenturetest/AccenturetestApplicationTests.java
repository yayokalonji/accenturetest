package com.test.accenturetest;

import com.test.accenturetest.model.Person;
import com.test.accenturetest.service.PersonService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccenturetestApplicationTests {

	@Autowired
	private PersonService personService;

	private Person person;

	@Before
	public void setup(){
		person = new Person("Homero","Simpson",40);
	}

	@Rule
	public Timeout timeout = Timeout.builder().withTimeout(10000L, TimeUnit.MILLISECONDS).build();

	@Test
	public void testByAll() {
		assertEquals(1 , personService.getAll().size());
	}

	@Test
	public void testByNameEquals(){
		assertNotEquals(person,personService.getByFirstName("Homero"));
	}

}
