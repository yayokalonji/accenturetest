package com.test.accenturetest.repository;

import com.test.accenturetest.model.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends MongoRepository <Person, String> {
    public Person findByFirstName(String firstName);
}
