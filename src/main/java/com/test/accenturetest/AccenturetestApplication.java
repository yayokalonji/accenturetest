package com.test.accenturetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccenturetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccenturetestApplication.class, args);
	}
}
